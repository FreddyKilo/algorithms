package com.freddykilo;

class HashFunction {

    public static void main(String[] args) {
        String s = "hello world";
        System.out.println(getHash(s, 24));
    }

    /**
     * Create a hash value of a String
     *
     * @param string
     * @return an n-digit hex String
     */
    public static long getHash(String string, int hashCodeLength) {
        String[] strArray = new String[hashCodeLength / 2];
        char[] charArray = string.toCharArray();
        int minLayers = 101;
        int maxIteration = (string.length() < strArray.length) ? strArray.length * 2 * minLayers : string.length() * minLayers;
        for (int i = 0; i < maxIteration; i++) {
            int sIndex = i % strArray.length;
            int cIndex = i % charArray.length;
            if (strArray[sIndex] == null) {
                strArray[sIndex] = getHexValue(charArray[cIndex]);
            } else {
                strArray[sIndex] = getHexValue((char) ((getIntValue(strArray[sIndex]) + charArray[cIndex] + i) % 256));
            }
        }
        StringBuilder hashCode = new StringBuilder();
        for (String s : strArray) {
            hashCode.append(s);
        }
        return getNumFromString(hashCode.toString());
    }

    private static String getHexValue(char c) {
        int charInt = c;
        String s = Integer.toHexString(charInt);
        if (s.length() == 1) return "0" + s;
        else return s;
    }

    private static int getIntValue(String s) {
        return Integer.parseInt(s, 16);
    }

    public static long getNumFromString(String s) {
        long total = 1;
        for (char c : s.toCharArray()) {
            if (c != 0) total *= c;
        }
        return total;
    }

}
