package com.freddykilo.CSC110;

import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Name: Freddy Kudelka
 * File: FileRDemo.java
 * Date: 10/20/2017
 * Course: CSC110
 */
public class FileRDemo {

    private static Map<String, String> studentScores;
    private static DecimalFormat formatter;

    public static void main(String[] s) throws IOException {
        //declare and instantiate the input file object and open the file
        Scanner inFile = new Scanner(new FileReader("src/com/freddykilo/CSC110/StudentGrades"));
        studentScores = new HashMap<>();
        formatter = new DecimalFormat("0.##");

        String line; //variable to put each line as read

        while (inFile.hasNextLine()) {
            //read a line of text from the file
            line = inFile.nextLine();

            parseScores(line);
        }
        //close the file
        inFile.close();

        print("Java Class Grade Report\n-----------------------\n");

        // print all results
        for (Map.Entry<String, String> student : studentScores.entrySet()) {
            print(student.getKey() + " has an exam average of " + student.getValue());
        }
    }

    private static void parseScores(String studentLine) {
        String[] entries = studentLine.split(",");
        // parse name
        String name = entries[0];
        // parse scores
        int[] scores = new int[entries.length - 1];
        for (int i = 0; i < scores.length; i++) {
            scores[i] = Integer.valueOf(entries[i+1]);
        }
        studentScores.put((name.charAt(0) + ". " + name.split(" ")[1]),
                getAverageScore(scores));
    }

    private static String getAverageScore(int[] scores) {
        double total = 0;
        for (int score : scores) {
            total += score;
        }
        return formatter.format(total / scores.length);
    }

    private static void print(String message) {
        System.out.println(message);
    }
}
