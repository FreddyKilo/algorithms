package com.freddykilo.CSC110;

//Program Name:  LoopConceptReview
//Author : Freddy Kudelka
//Date: 10/2/2017
//Description : Review of loops with a series of examples.
//              Fill in code as indicated by comments.
//              Output should match.

import java.util.Scanner;

public class LoopConceptReview {

    public static void main(String[] args) {
        int value = 0;
        String evenOrOdd = "***";
        String word = "\n";
        Scanner scan = new Scanner(System.in);
        System.out.println("Loop Concept Review");

        //#1 Using a while loop, write an input validation loop that asks users to enter a positive value
        System.out.println("\n#1. Input validation: ");
        do {
            System.out.println("Positive values only.");
            System.out.print("Please enter a value: ");
            value = scan.nextInt();
        } while (value < 0);
        System.out.println("Correct. " + value + " is a positive number.");


        //#2 write a for loop that displays all of the odd numbers, 1 through 19
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n#2. Odd numbers: ");
        for (int i = 0; i < 20; i++) {
            if (i % 2 > 0) {
                stringBuilder.append(String.valueOf(i) + " ");
            }
        }
        System.out.println(stringBuilder.toString());


        //#3 write a for loop that displays every fifth number, 0 through 50
        stringBuilder.setLength(0);
        stringBuilder.append("\n#3. Every fifth number: ");
        for (int i = 0; i <= 50; i++) {
            if (i % 5 == 0) {
                stringBuilder.append(String.valueOf(i) + " ");
            }
        }
        System.out.println(stringBuilder.toString());


        //#4 write a for loop that counts down from 15 to 0
        stringBuilder.setLength(0);
        stringBuilder.append("\n#4. Count down: ");
        for (int i = 15; i >= 0; i--) {
            stringBuilder.append(String.valueOf(i) + " ");
        }
        System.out.println(stringBuilder.toString());


		/*#5 write a nested for loop that creates a triangle that looks like this

		A
		AA
		AAA
		AAAA
		AAAAA

		*/
        stringBuilder.setLength(0);
        System.out.println("\n#5. Cool Triangle:");

        // no nested for loop needed
        for (int i = 1; i <= 5; i++) {
            stringBuilder.append("A");
            System.out.println(stringBuilder.toString());
        }

		/*#6 write a while loop that asks the user for value
         *   and displays whether the value is even or odd until
		 *   the user enters a 0. (called a sentinel value)
		 */
        System.out.println("\n#6. Even or Odd:");
        do {
            System.out.print("Enter a value (0 to quit): ");
            value = scan.nextInt();
            if (value == 0) System.out.println("");
            else if (value % 2 == 0) System.out.println(String.valueOf(value) + " is even");
            else System.out.println(String.valueOf(value) + " is odd");
        } while (value != 0);



		/*#7 Using a do..while loop, ask the user for a word and
		 * display the word until the user enters either "stop"
		 * or "STOP" or "Stop" or "sTop" or "stoP".
		 * (hint: use the String method equalsIgnoreCase)
		 */
        System.out.print("\n#7. Words!!");
        do {
            System.out.println(word);
            System.out.print("Enter a word (stop to quit): ");
            word = scan.next();
        } while (!word.equalsIgnoreCase("stop"));

        System.out.println("\nCompleted the Loop Concept Review");
    }

}

/*
Expected Output.  Yours much match.

Loop Concept Review

#1. Input validation:
Positive values only.
Please enter a value: -3
Positive values only.
Please enter a value: 0
Positive values only.
Please enter a value: 5
Correct. 5 is a positive number.

#2. Odd numbers: 1 3 5 7 9 11 13 15 17 19
#3. Every fifth number: 0 5 10 15 20 25 30 35 40 45 50
#4. Count down: 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
#5. Cool Triangle:
A
AA
AAA
AAAA
AAAAA

#6. Even or Odd:
Enter a value (0 to quit):13
13 is odd
Enter a value (0 to quit):6
6 is even
Enter a value (0 to quit):0

#7. Words!!
Enter a word:Hello
The word is: Hello
Enter a word:students
The word is: students
Enter a word:stop
The word is: stop

Completed the Loop Concept Review
*/
