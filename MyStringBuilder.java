package com.freddykilo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyStringBuilder {

    private char[] allChars;
    private int nextIndex = 0;
    private static final double INITIAL_ARRAY_SIZE = Math.pow(2, 1);
    private static final int WORDS_MULTIPLIER = 20;

    public MyStringBuilder() {
        allChars = new char[(int) INITIAL_ARRAY_SIZE];
    }

    /**
     * Compare the runtime of MyStringBuilder vs String concatenation
     * @param args
     */
    public static void main(String[] args) {

        MyStringBuilder myStringBuilder = new MyStringBuilder();
        StringBuilder stringBuilder = new StringBuilder();
        List<String> manyStrings = getStrings();
        for (int i = 0; i < WORDS_MULTIPLIER; i++) {
            manyStrings.addAll(getStrings());
        }

        // Test with StringBuilder
        long start = System.currentTimeMillis();
        for (String s : manyStrings) {
            stringBuilder.append(s);
        }
        System.out.println("Time to finish StringBuilder:   " + (System.currentTimeMillis() - start) + "ms");

        // Test with MyStringBuilder
        start = System.currentTimeMillis();
        for (String s : manyStrings) {
            myStringBuilder.append(s);
        }
        System.out.println("Time to finish MyStringBuilder: " + (System.currentTimeMillis() - start) + "ms");

//         Test with concat
        start = System.currentTimeMillis();
        String concatString = "";
        for (String s : manyStrings) {
            concatString += s;
        }
        System.out.println("Time to finish String concat: " + (System.currentTimeMillis() - start) + "ms");
    }

    private void append(String someString) {
        // Get each char from string and add them to allChars
        char[] tempCharArray = someString.toCharArray();

        // add all chars from someString to allChars starting from the next empty index
        for (char someChar : tempCharArray) {

            // if there is no empty index, double the size of the array
            if (nextIndex == allChars.length) allChars = Arrays.copyOf(allChars, allChars.length * 4);

            allChars[nextIndex] = someChar;
            nextIndex++;
        }
    }

    public String toString() {
        return new String(allChars);
    }

    private static List<String> getStrings() {
        List<String> strings = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader("/Users/fred.kudelka/projects/algorithms/src/com/freddykilo/words");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            for (String s : bufferedReader.readLine().split(" ")) {
                strings.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strings;
    }

}
