package com.freddykilo;

public class ValuesMapper {

    public static void main(String[] args) {
        System.out.println(mapValues(50, 15, 100, 0, 100));
    }

    private static double mapValues(double input, double inOne, double inTwo, double outOne, double outTwo) {
        return (outTwo - outOne) / (inTwo - inOne) * (input - inOne) + outOne; // mx + b
    }

}
